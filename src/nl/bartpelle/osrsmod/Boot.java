package nl.bartpelle.osrsmod;

import EDU.purdue.cs.bloat.cfg.Block;
import EDU.purdue.cs.bloat.cfg.FlowGraph;
import EDU.purdue.cs.bloat.context.PersistentBloatContext;
import EDU.purdue.cs.bloat.editor.*;
import EDU.purdue.cs.bloat.file.ClassFileLoader;
import EDU.purdue.cs.bloat.reflect.ClassInfo;
import EDU.purdue.cs.bloat.reflect.ClassInfoLoader;
import EDU.purdue.cs.bloat.reflect.Constant;
import EDU.purdue.cs.bloat.reflect.MethodInfo;
import EDU.purdue.cs.bloat.tree.*;
import jode.decompiler.Decompiler;
import jode.decompiler.ProgressListener;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipFile;

/**
 * Created by Gebruiker on 20-1-2015.
 */
public class Boot {

	public static void main(String[] args) throws Exception {
		ClassFileLoader loader = new ClassFileLoader();
		PersistentBloatContext context = new PersistentBloatContext(loader);
		ClassInfo[] infos = loader.loadClassesFromZipFile(new ZipFile("osrs.jar"));

		/* Load classes */
		Set<ClassEditor> editorSet = new HashSet<>();
		for (ClassInfo i : infos) {
			if (i != null)
				editorSet.add(context.editClass(i));
		}

		/* Discover methods */
		Set<MethodEditor> xteaCandidates = new HashSet<>();
		for (ClassEditor e : editorSet) {
			for (MethodInfo i : e.methods()) {
				if ((i.modifiers() & 0x8) != 0 && (i.modifiers() & 0x10) != 0) {
					MethodEditor me = context.editMethod(i);
					if (me.paramTypes().length == 2 && me.paramTypes()[0] == Type.BOOLEAN) {
						System.out.println("Candidate: " + e.name() + "." + me.name());
						xteaCandidates.add(me);
					}
				}
			}
		}

		/* Identify candidates */
		final FlowGraph[] likely = {null};
		final MethodEditor[] m = {null};
		for (MethodEditor candidate : xteaCandidates) {
			System.out.println(candidate.declaringClass().name() + "." + candidate.name());

			FlowGraph cfg = new FlowGraph(candidate);
			cfg.visit(new TreeVisitor() {
				@Override
				public void visitConstantExpr(ConstantExpr expr) {
					if (expr.value() != null && expr.value().equals(148)) {
						likely[0] = cfg;
						m[0] = candidate;
					}
					super.visitConstantExpr(expr);
				}
			});
		}

		AtomicReference<MemberRef> xteaArray = new AtomicReference<>();
		if (likely[0] != null) {
			/* Discover the xtea array field */
			likely[0].visit(new TreeVisitor() {
				@Override
				public void visitNewMultiArrayExpr(NewMultiArrayExpr expr) {
					if (expr.dimensions().length > 1) {
						if (expr.dimensions()[1] instanceof ConstantExpr && expr.stmt() instanceof ExprStmt) {
							if (((ConstantExpr) expr.dimensions()[1]).value().equals(4) && ((ExprStmt) expr.stmt()).expr() instanceof StoreExpr) {
								StoreExpr storeExpr = (StoreExpr) ((ExprStmt) expr.stmt()).expr();
								if (storeExpr.defs()[0] instanceof StaticFieldExpr) {
									xteaArray.set(((StaticFieldExpr) storeExpr.defs()[0]).field());
								}
							}
						}
					}

					super.visitNewMultiArrayExpr(expr);
				}
			});


			AtomicBoolean b = new AtomicBoolean();
			likely[0].visit(new TreeVisitor() {
				@Override
				public void visitCallMethodExpr(CallMethodExpr expr) {
					if (expr.params().length > 0 && expr.params()[0].type() == Type.STRING) {
						likely[0].domParent(likely[0].domParent(expr.block())).visit(new TreeVisitor() {
							@Override
							public void visitStmt(Stmt expr) {
								if (expr instanceof ExprStmt && !b.get()) {
									if (((ExprStmt) expr).expr() instanceof StoreExpr) {
										StoreExpr se = ((StoreExpr) ((ExprStmt) expr).expr());
										if (se.defs()[0] instanceof ArrayRefExpr && se.expr() instanceof LocalExpr) {
											ArrayRefExpr ar = ((ArrayRefExpr) se.target());
											createInjectable(se.expr(), expr, ar.index(), xteaArray.get());
											//b.set(true);
											System.out.println(expr + ". " + se.expr().getClass());
										}
									}
								}

								super.visitStmt(expr);
							}
						});
						//return;
					}
					super.visitCallMethodExpr(expr);
				}
			});

			likely[0].commit();
			m[0].commit();
			m[0].declaringClass().commit();
			m[0].declaringClass().classInfo().commit();

			Decompiler d = new Decompiler();
			d.setClassPath(new File("").getAbsolutePath());
			//d.decompile("v", new PrintWriter(System.out), (a, bb) -> { });
		}
	}

	/*
	   536: iconst_1                            // Push 1
       537: anewarray java/lang/Object          // New array of Object[]
       540: astore 17                           // Store into local var 17
       542: aload 17                            // Get local var 17
       544: iconst_0                            // Push 0
       545: iload 11                            // Get local integer 11 (region id)
       547: invokestatic Integer.valueOf:(I)Ljava/lang/Integer;
       550: checkcast java/lang/Object          // Cast check to (Object)
       553: aastore                             // Store into index 0 of our array
       554: getstatic System.out:Ljava/io/PrintStream;
       557: ldc_w "Mapkeys for %d \n"
       560: aload 17                            // Load array (local obj 17)
       562: invokevirtual java/lang/System.printf:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	 */

	static List done = new LinkedList<>();
	private static void createInjectable(Expr e, Stmt after, Expr indexExpr, MemberRef xteaArray) {
		if (done.contains(after))
			return;

		Type printStream = Type.getType(PrintStream.class);
		Type system = Type.getType(System.class);
		Type integerType = Type.getType(Integer.class);
		Tree tree = after.block().tree();

		StaticFieldExpr getXteas = new StaticFieldExpr(xteaArray, Type.INTEGER.arrayType(2));
		ArrayRefExpr getOneOfIt = new ArrayRefExpr(getXteas, indexExpr, Type.INTEGER.arrayType(), Type.INTEGER.arrayType());
		CallStaticExpr xteacall = new CallStaticExpr(new Expr[] {e, getOneOfIt}, new MemberRef(Type.getType(OSRSMod.class),
				new NameAndType("printXteas", Type.getType(new Type[] {Type.INTEGER, Type.INTEGER.arrayType()}, Type.VOID))), Type.VOID);

		ExprStmt ex = new ExprStmt(xteacall);
		tree.addStmtAfter(ex, after);
		System.out.println("Injected!");

		done.add(after);
		done.add(ex);
	}

	public static void testtest() {

	}

	private static Expr intExpr(int v) {
		return new ConstantExpr(v, Type.INTEGER);
	}

	private static Expr[] exprList(Expr... exprs) {
		return exprs;
	}

}
